# README

The [ticket template](ticket-template.md) in this repository contains a template I started when I started here at GitLab. I've been iterating on it and have made it public in the hopes that others find it useful. Please note that this is not meant to be a one size fits all (or even most) approach but I hope you derive some value nonetheless.

When a ticket goes through multiples responses, I copy the **Question**, **Research**, **Summary** and **Response** sections at the end of the document and keep going. 

## Problem Statement
> The solution is only as good as the problem statement. 

I aim to keep an evolving description of the problem statement in the beginning of my responses to customers. This gives them the opportunity to say 'No, you have not understood the problem correclty', it makes it easier for other engineers to pick the ticket up and it makes summarizing and closing the ticket at the end much easier. 