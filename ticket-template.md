---
title: "XXXXXX: Ticket Subject"
keywords:
  - GitLab
  - Template
  - Tickets
  - Federal? Regular?
  - Stage? Create/Verify/Plan
---
--> You can ignore this line and the ones above if you are not using [Zettlr](https://www.zettlr.com/). 


# XXXXXX: Ticket Subject

## Question
I copy the customer's question over to this section to make it easy to refer to without leaving the current application. 

## Research
You don't need to fill out each question for each ticket but I've found it helpful to have these all listed out so I remember. 

  - [ ] Is there a clear problem statement? (Aim to verify the accuracy of the)
  - [ ] Do you need additional information?
  - [ ] Does this user's license definitely grant them access to this feature?
  - [ ] Are there other similar tickets in ZenDesk?
  - [ ] Can you reproduce the problem?
  - [ ] Did this ever work? Does it misbehave consistently?
  - [ ] Have you looked through the issue tracker?
  - [ ] Consider: what led this customer to file this ticket? (Do a readthrough for context clues. Does it sound like they tried other things first? How were they solving this problem previously? etc.)
  - [ ] Have you looked through the customer's previous tickets?
  - [ ] Would it make sense to inquire as to **why** the customer is trying to do what they are trying to do?
      - [ ] Sometimes the answer may be irrelevant.
      - [ ] Sometimes the answer may lead you to propose an alternate and better solution

### Reproducing the Error

### Summary of Findings
This section includes a summary of what you found during research and reproduction. Ideas worth pursuing go here. Other ideas stay up there. 

## Response

